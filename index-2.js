/**
 * Created by djavrell on 07/05/16.
 */

// (\[[a-zA-Z0-9]*\])|(vostfr)|(hd) / gi => modifier

var _ = require('lodash');
var fs = require('fs');
var wget = require('wget-improved');
var pace  = require('pace')(100);
var status = require('node-status');
var rxjs = require('rxjs');
var Rx = require('rxjs/Rx');

var Observable = require('rxjs/Observable').Observable;

var tab = fs.readFileSync(process.argv[2]).toString().split("\n");
tab = _.pull(tab, '');

var cor = {
    "%2520": " ",
    "%255B": "[",
    "%255D": "]",
    "%2540": "@",
    ".MaChO@zone-telechargement.com": "",
    "[HorribleSubs]": "",
    "[Despair-Paradise]": "",
    "[720p]": ""
  };

function clean(key) {
  var str = _.last(_.split(key, '/'));

  _(cor).forEach(function (key, val) {
    while (str.indexOf(val) > -1) {
      str = _.replace(str, val, key);
    }
    str = _.trim(str, ' ');
  });
  return str;
}

function dl_link(src) {
  var op = clean(src);

  var state = 0;
  var barre = status.addItem(op, {
    type: ["bar", "percentage"],
    max: 100
  });

  var dl = wget.download(src, op, {});
  dl.on('start', function (size) {
    status.start();
    status.cellCount(size);
    state = 0;
  });
  dl.on('error', function(err) {
    console.error(err.toString);
  });
  dl.on('end', function(output) {
    console.info(output);
    status.stop();
  });
  dl.on('progress', function(progress) {
    barre.inc(progress - state);
    state = progress;
    // state += progress;
    // pace.op(progress * 100);
    // console.log(op + " => " + progress);
  });
}

var obs = Rx.Observable.from(tab);

var obr = {
    next: data => dl_link(data),
    error: err => console.error(err),
    complete: () => console.log("done")
};

obs.subscribe(obr);
